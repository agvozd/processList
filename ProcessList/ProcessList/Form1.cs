﻿using ProcessList.UserControls;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace ProcessList
{
    public class Test
    {
        public Process _Process { get; set; }
        public PropertyInfo _PropertyInfo { get; set; }

        public Test(Process process, PropertyInfo properyInfo)
        {
            _Process = process;
            _PropertyInfo = properyInfo;
        }
    }
     
    public partial class ProcessListForm : Form
    {
        public ProcessListForm()
        {
            InitializeComponent();

            processListControl.processList.View = View.Details;
            processListControl.processList.FullRowSelect = true;
            processListControl.SelectedIndexChanged += new EventHandler(processListControl_SelectedIndexChanged);
          
        }

        ProcessListHelper processListHelper = new ProcessListHelper();

        protected void processListControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            ProcessListControl processListControl = (ProcessListControl)sender;
            if (processListControl.properiesList.SelectedIndex == 0)
            {
                return;
            }

            processListControl.SortParameter = processListControl.properiesList.SelectedItem.ToString();
            ProcessInfo processInfo = SortByProperty(processListControl.SortParameter);
            
            DisplayProcessList(processInfo, processListControl.SortParameter);           
        }

        private ProcessInfo SortByProperty(string property)
        {            
            Process[] processList = processListHelper.GetProcessList().ToArray();
            string[] propertiesList = processListHelper.GetProperties();

            Process[] processArray = new Process[processList.Length];
            if (!string.IsNullOrEmpty(property))
            {
                PropertyInfo propertyInfo = typeof(Process).GetProperty(property);

                processArray = processList.OrderBy(process => GetPropertyValue(process, propertyInfo)).ToArray();
                processArray = processList;
                //processArray= processList.OrderBy(process => propertyInfo.GetValue(process, null)).ToArray();                
            }
            else
            {
                processArray = processList;
            }

            ProcessInfo processInfo = new ProcessInfo(processArray, propertiesList);
            return processInfo;
        }

        public object GetPropertyValue(Process process, PropertyInfo propertyInfo)
        {
            try
            {                
                return propertyInfo.GetValue(process, null); 
            }
            catch (Exception exp)
            {
                Type type = propertyInfo.PropertyType;

                if (type.Equals(typeof(int)))
                {
                    return 0;
                }

                if (type.Equals(typeof(string)))
                {
                    return string.Empty;
                }
       
                return DateTime.MinValue;
            }
        }

        private ProcessInfo InitProcessList()
        {
            ProcessInfo processInfo = new ProcessInfo();
            processInfo.ProcessList = processListHelper.GetProcessList();
            processInfo.PropertiesList = processListHelper.GetProperties();

            return processInfo;            
        }

        private void DisplayProcessList(ProcessInfo processInfo, string selectedPropery="")
        {
            RefreshProcessListControl();

            foreach (var process in processInfo.ProcessList)
            {
                string processName = string.Empty;
                string processID = string.Empty;
                string startTime = string.Empty;

                try { processName = process.ProcessName; }
                catch { }

                try { processID = process.Id.ToString(); }
                catch { }

                try { startTime = process.StartTime.ToString(); }
                catch { }

                processListControl.processList.Items.Add(new ListViewItem(new string[] { processName, processID, startTime }));
            }

            processListControl.properiesList.Items.AddRange(processInfo.PropertiesList);           
        }

        private void RefreshProcessListControl()
        {
            processListControl.processList.Items.Clear();
            processListControl.properiesList.Items.Clear();
        }

        private void ProcessListForm_Load(object sender, EventArgs e)
        {
            //Timer timer = new Timer();
            //timer.Interval = (10 * 1000); // 10 secs
            //timer.Tick += new EventHandler(timer_Tick);
            //timer.Start();

            ProcessInfo processInfo= InitProcessList();
            DisplayProcessList(processInfo);

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            ProcessInfo processInfo = new ProcessInfo();

            string sortProperty = processListControl.SortParameter;

            if (string.IsNullOrEmpty(sortProperty))
            {
                processInfo = InitProcessList();               
            }
            else
            {
                processInfo = SortByProperty(sortProperty);
            }

            DisplayProcessList(processInfo);
        }
    }
}
