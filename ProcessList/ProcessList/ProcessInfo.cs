﻿using System;
using System.Diagnostics;

namespace ProcessList
{
    class ProcessInfo
    {
        public Process[] ProcessList { get; set; }
        public string[] PropertiesList { get; set; }  

        public ProcessInfo() { }
        
        public ProcessInfo(Process[] processList, string[] propertiesList)
        {
            ProcessList = processList;
            PropertiesList = propertiesList;
        }
      
    }
}
