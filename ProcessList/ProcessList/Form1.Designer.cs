﻿namespace ProcessList
{
    partial class ProcessListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcessListForm));
            this.processListControl = new ProcessList.UserControls.ProcessListControl();
            this.SuspendLayout();
            // 
            // processListControl
            // 
            this.processListControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.processListControl.Location = new System.Drawing.Point(12, 12);
            this.processListControl.Name = "processListControl";
            this.processListControl.Size = new System.Drawing.Size(776, 426);
          
            this.processListControl.TabIndex = 0;
            // 
            // ProcessListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.processListControl);
            this.Name = "ProcessListForm";
            this.Text = "ProcessList";
            this.Load += new System.EventHandler(this.ProcessListForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private UserControls.ProcessListControl processListControl;
    }
}

