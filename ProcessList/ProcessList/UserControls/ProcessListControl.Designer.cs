﻿namespace ProcessList.UserControls
{
    partial class ProcessListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.processList = new System.Windows.Forms.ListView();
            this.processName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.processId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.startTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.properiesList = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // processList
            // 
            this.processList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.processList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.processName,
            this.processId,
            this.startTime});
            this.processList.Location = new System.Drawing.Point(0, 26);
            this.processList.Name = "processList";
            this.processList.Size = new System.Drawing.Size(384, 269);
            this.processList.TabIndex = 0;
            this.processList.UseCompatibleStateImageBehavior = false;
            // 
            // processName
            // 
            this.processName.Text = "Name";
            this.processName.Width = 150;
            // 
            // processId
            // 
            this.processId.Text = "ID";
            this.processId.Width = 150;
            // 
            // startTime
            // 
            this.startTime.Text = "Start time";
            this.startTime.Width = 150;
            // 
            // properiesList
            // 
            this.properiesList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.properiesList.FormattingEnabled = true;
            this.properiesList.Location = new System.Drawing.Point(0, 4);
            this.properiesList.Name = "properiesList";
            this.properiesList.Size = new System.Drawing.Size(384, 21);
            this.properiesList.TabIndex = 1;
            this.properiesList.SelectedIndexChanged += new System.EventHandler(this.comboBox_SelectedIndexChanged);
            // 
            // ProcessListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.properiesList);
            this.Controls.Add(this.processList);
            this.Name = "ProcessListControl";
            this.Size = new System.Drawing.Size(384, 298);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.ListView processList;
        public System.Windows.Forms.ComboBox properiesList;
        public System.Windows.Forms.ColumnHeader processName;
        public System.Windows.Forms.ColumnHeader processId;
        public System.Windows.Forms.ColumnHeader startTime;
    }
}
