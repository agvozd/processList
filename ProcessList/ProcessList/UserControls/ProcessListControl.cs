﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProcessList.UserControls
{
    public partial class ProcessListControl : UserControl
    {
        public ProcessListControl()
        {
            InitializeComponent();
        }

        [Description("Test text displayed in the textbox"), Category("Data")]
        public string SortParameter { get; set; } = string.Empty;

        public event EventHandler SelectedIndexChanged;

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bubble the event up to the parent
            if (SelectedIndexChanged != null)
                SelectedIndexChanged(this, e);
        }
    }
}
