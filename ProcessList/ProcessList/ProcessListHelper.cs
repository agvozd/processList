﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace ProcessList
{
    class ProcessListHelper
    {
        public Process[] GetProcessList()
        {
            // Get all processes running on the local computer.
            Process[] localProcessAll = Process.GetProcesses();

            return localProcessAll;
        }

        public string[] GetProperties()
        {
            HashSet<string> result = new HashSet<string>();

            try
            {
                Process testProcess = Process.GetCurrentProcess();

                Type type = testProcess.GetType();

               PropertyInfo[] processPropertiesArray = type.GetProperties();

                foreach (var property in processPropertiesArray)
                {
                    if (property.PropertyType == typeof(string))
                    {
                        result.Add(property.Name);
                        continue;
                    }

                    if (property.PropertyType == typeof(int))
                    {
                        result.Add(property.Name);
                        continue;
                    }

                    if (property.PropertyType == typeof(DateTime))
                    {
                        result.Add(property.Name);
                        continue;
                    }
                }
            }
            catch(Exception exp)
            {
                return null;
            }

            string[] propertiesArray = new string[result.Count];
            result.CopyTo(propertiesArray);

            return propertiesArray;

        }
    }
}
